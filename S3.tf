resource "aws_s3_bucket" "bucket" {
  bucket = "lakshmipathibucket33"
  acl    = "private"
   
   lifecycle{
     prevent_destroy= true
   }
   versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
}



resource "aws_dynamodb_table" "statefiledb" {
  name             = "statefiledb"
  hash_key         = "TestTableHashKey"
  billing_mode     = "PAY_PER_REQUEST"
  
  attribute{
    name = "LockID"
    type ="S"
  }
}

terraform {
  backend "s3" {
    bucket = "lakshmipathibucket33"
    key    = "tfstatefile/s3/key"
    region = "us-east-1"
    dynamodb_table = "statefiledb"
    encrypt = true
  }
}